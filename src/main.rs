#![no_std]  // bare metal rust core
#![no_main] // disable rust entry point
#![feature(panic_implementation, start)]

extern crate uefi;
extern crate uefi_services;
extern crate uefi_exts;

// already in uefi-services
// use core::panic::PanicInfo;

use uefi::{Handle};
use uefi::table;
use uefi::Status;

// #[panic_implementation]
// fn panic(_panic_info: &PanicInfo) -> ! { loop {} }

#[no_mangle]
pub extern "C" fn _start() -> ! { loop {} }

// #[start]
// fn main(_argc: isize, _argv: *const *const u8) -> isize {
//     0
// }

#[no_mangle]
pub extern "C" fn uefi_start(_handle: Handle, _system_table: &'static table::SystemTable) -> Status {
    uefi::Status::Success
}