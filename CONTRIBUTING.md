# Contributing

## Creating a bare metal rust binary

- Philipp Opperman, by providing ["Writing an OS in Rust"](https://os.phil-opp.com/)

## Booting the kernel (via UEFI)

- Eric Holk for his ["Booting to rust"](https://blog.theincredibleholk.org/blog/2013/11/18/booting-to-rust/) to attempting to uefi boot.
- Gabriel Majeri for the [UEFI rust wrapper](https://github.com/GabrielMajeri/uefi-rs)
- Philipp Oppermann for additional information on [BIOS boot](https://os.phil-opp.com/minimal-rust-kernel/)