# colonel

Digging into the kernel space by attempting to write a simple kernel in rust with framebuffer.

## error messages and the tutorial

This can also be seen as an error driven step-by-step-tutorial. By stating to use the rust core
library

```rust
#[no_std]
```
the compiler throws the first error message. By solving the errors, you can proceed to the next step.

A more detailed description of each error can be found at [Writing an OS in Rust (Second Edition) by Philipp Oppermann](https://os.phil-opp.com/).

Steps 1-6 are required to create a [freestanding / bare metal rust binary](https://os.phil-opp.com/freestanding-rust-binary/), that uses the core library
only and prepares the binary to run with a different non-rust entry point that can be invoked during
the boot process (pre-kernel boot).

Steps 7- attempt to UEFI boot the kernel using sources of Philipp Oppermann's ["Minimal Rust Kernel"](https://os.phil-opp.com/minimal-rust-kernel/) tutorial and Gabriel Majeri's [uefi-rs wrapper](https://github.com/GabrielMajeri/uefi-rs).


## SECTION 1 - A bare metal rust binary

### 1 Using the rust core only (#[no_std]) misses panic implementations:

```rust
error: `#[panic_implementation]` function required, but not found

error: language item required, but not found: `eh_personality`

error: aborting due to 2 previous errors

error: Could not compile `colonel`.
```

**Solution**: add following to Cargo.toml and main.rs

```bash
[profile.dev]
panic = "abort"

[profile.release]
panic = "abort"
```

```rust
#![feature(panic_implementation)]

use core::panic::PanicInfo;

#[panic_implementation]
fn panic(_panic_info: &PanicInfo) -> ! { loop {} } // disregard RLS mesg: duplicate lang item
```

### (1.a Visual code says panic implemenation found twice)

```bash
[rustc]
duplicate lang item found: `panic_impl`.

note: first defined in crate `std`.
fn (_panic_info: &PanicInfo) -> !
fn (_panic_info: &PanicInfo) -> !
```

**Solution**: Disregard RLS message (using Visual Studio Code)

### 2 cargo build failed using stable release channel

```bash
error[E0554]: \#![feature] may not be used on the stable release channel
 --> src/main.rs:3:1
  |
3 | #![feature(panic_implementation, start)]
  | ^^^^^^^^^^
```

**Solution**: requires nightly

```bash
    $ rustup override set nightly
```

### 3 Language item "start" (in rust core) not found

**Note**: This and step 4 are not the correct solution. Step 6 is and step 5 comes after step 6.

```bash
error: requires `start` lang_item

error: aborting due to previous error

error: Could not compile `colonel`.
```

**Solution**: Add "start"to used language features

```rust
#![feature(panic_implementation, start)]
```

and add #[start] to function used on start

```rust
#[start]
fn main() {}
```

### 4 The start function requires three arguments

```rust
error[E0308]: start function has wrong type
  --> src/main.rs:11:1
   |
11 | fn main() {}
   | ^^^^^^^^^ incorrect number of function parameters
   |
   = note: expected type `fn(isize, *const *const u8) -> isize`
              found type `fn()`

error: aborting due to previous error
```

**Solution**: Add arguments and return type, return 0

```rust
fn main(_argc: isize, _argv: *const *const u8) -> isize {
    0
}
```

### 5 Compile attempt causes "linkink with 'cc'" error

```rust
error: linking with `cc` failed: exit code: 1
  |
  = note: "cc" "-Wl,--as-needed" "-Wl,-z,noexecstack" "-m64" "-L"  ...
  ...
= note: /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../lib/Scrt1.o: In function `_start':
          (.text+0x12): undefined reference to `__libc_csu_fini'
          (.text+0x19): undefined reference to `__libc_csu_init'
          (.text+0x26): undefined reference to `__libc_start_main'
          collect2: error: ld returned 1 exit status
        

error: aborting due to previous error

error: Could not compile `colonel`.
```

**Solution**: Compile with:

```rust
$ cargo rustc -- -Z pre-link-arg=-nostartfiles
```

### 6 Disabling rust entry point

For the kernel it is necessary to disable the rust entry point, that is the main fn and replace it
with the _start function named according to the C Calling Convention.

**Solution**: Add the following and remove or comment your fn main

```rust
#![no_main]

#[no_mangle]
pub extern "C" fn _start() -> ! {
    loop {}
}
```

## SECTION 2 - Booting the Colonel using UEFI boot

### 7 Create the target specification

Phillip Oppermann provides a target specification for unknown-linux without uefi support. Gabriel Manjeri provides a runner for a Windows target specification and UEFI. Colonel is supposed to use
a unknown-linux target with UEFI support.

**Solution**: Create a target specification in json format. Name it "x86
_64-colonel-uefi.json"

**Note**: 

- Change the field "os" to "uefi".
- Adding panic-strategy" : "abort" allows us to remove the snippets in our Cargo.toml.
- For non Windows PC change the field "llvm-target" and "is-like-windows"

```rust
{
    "llvm-target": "x86_64-unknown-none",
    "target-endian": "little",
    "target-pointer-width": "64",
    "target-c-int-width": "32",
    "os": "none",
    "arch": "x86_64",
    "data-layout": "e-m:e-i64:64-f80:128-n8:16:32:64-S128",
    "linker": "rust-lld",
    "linker-flavor": "lld-link",
    "pre-link-args": {
      "lld-link": [
        "/Subsystem:EFI_Application",
        "/Entry:uefi_start"
      ]
    },
    "panic-strategy": "abort",
    "default-hidden-visibility": true,
    "executables": true,
    "position-independent-executables": true,
    "exe-suffix": ".efi",
    "is-like-windows": false,
    "emit-debug-gdb-scripts": false
  }
```

### 8 Clone uefi-wrapper by Garbiel Majeri

The kernel is supposed to be loaded by the UEFI.

```rust
$ git clone "https://github.com/GabrielMajeri/uefi-rs.git"
```

### 8 Add a new entry point for the UEFI

```rust
#[no_mangle]
pub extern "C" fn uefi_start(_handle: Handle, _system_table: &'static table::SystemTable) -> Status {
    uefi::Status::Success
}
```

### 9 Add the uefi dependencies to Cargo.toml

**Note**: It's probably better to separate the uefi-boot from the kernel project later.

```rust
uefi = { path = "../uefi-rs" }
uefi-services = { path = "../uefi-rs/uefi-services" }
uefi-exts = { path = "../uefi-rs/uefi-exts" }

log = { version = "0.4", default-features = false }
```

### 10 Compile the core library and the kernel for the new the custom target

```rust
$ cargo xbuild --target x86_64-colonel-uefi.json
```

### 11 /Machine is not specified

```rust
error: linking with `rust-lld` failed: exit code: 1
  |
  = note: "rust-lld" "-flavor" "link" "/Subsystem:EFI_Application" "/Entry:uefi_start"  ...
  ...
  = note: rust-lld: error: 
    ...
          rust-lld: warning: /machine is not specified. x64 is assumed
          

error: aborting due to previous error

error: Could not compile `colonel`.
```

**Solution**: Add the field "/Machine:X64" to 

```rust
"pre-link-args": {
      "lld-link": [
        ...
        "/Machine:X64"
```